/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import controlador.Controlador;
import java.util.Scanner;
import modelo.Grupo;

/**
 * @author Rafael Partida Ibáñez (1516Ceed72)
 * @email rpartida81@yahoo.es
 * @fecha de creación 23-nov-2015
 */
public class VistaGrupo implements IVista<Grupo> {
    private String id;
    private String nombre;
    
    public void mostrar(Grupo grupo){
        System.out.println("***********");
        System.out.println("MOSTRAR GRUPO");
        System.out.println("***********");
        System.out.println("Id: " + grupo.getId());
        System.out.println("Nombre: " + grupo.getNombre());        
    }
    
    public Grupo obtener(){
        Grupo grupo = new Grupo();
        System.out.println("OBTENER GRUPO.");
        Scanner scn = new Scanner (System.in);
        System.out.print("Id: ");        
        id = scn.nextLine();
        grupo.setId(id);
        System.out.print("Nombre: ");
        nombre = scn.nextLine();
        grupo.setNombre(nombre);
        
        return grupo;
   }
    

}
