/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

/**
 * @author Rafael Partida Ibáñez (1516Ceed72)
 * @email rpartida81@yahoo.es
 * @fecha de creación 23-nov-2015
 */
public class Configurador {


    private static String titulo;
    private static Configurador miconfigurador;

    public static Configurador getConfigurador() {
        if (miconfigurador == null) {
            miconfigurador = new Configurador();
            System.out.println("Creada instancia Configurador");
        } else {
            System.out.println("Error : Solo se permite una instancia ");
        }
        return miconfigurador;
    }

    private Configurador() {
        titulo = "PRACTICA CLASES";
    }

    /**
     * @return the titulo
     */
    public static String getTitulo() {
        return titulo;
    }
}
